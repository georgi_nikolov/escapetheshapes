﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public Canvas gameOverCanvas;

    private void Start()
    {
        //Physics.gravity = new Vector3(0, -40.0F, 0);
    }

    public void EndGame()
    {
        EnemySpawn spawnManager = FindObjectOfType<EnemySpawn>();
        spawnManager.StopSpawning();
        PlayerMovement player = FindObjectOfType<PlayerMovement>();
        player.StopReceivingInput();
        gameOverCanvas.gameObject.SetActive(true);
    }

    public void RestartGame()
    {

    }
}
