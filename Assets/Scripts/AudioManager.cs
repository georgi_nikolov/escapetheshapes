﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {

    private static AudioManager instance;

    public Toggle toggle;
    public AudioSource audioSource;

    public static AudioManager Instance
    {
        get { return instance; }
    }
    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        toggle.onValueChanged.AddListener(OnToggleValueChanged);
    }

    private void OnToggleValueChanged(bool isOn)
    {
        if (!isOn)
        {
            Debug.Log("Play");

            audioSource.Play();
        }
        else
        {
            Debug.Log("Pause");
            audioSource.Pause();
        }
    }
}
