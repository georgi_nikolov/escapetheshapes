﻿using UnityEngine;

public class PlayerCollisionBehaviour : MonoBehaviour {

    public int health = 3;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag.Equals("Enemy"))
        {
            health -= 1;
            changeColor();
        }
        if(health <= 0)
        {
            FindObjectOfType<GameManager>().EndGame();
        }
    }

    void changeColor()
    {
        Renderer rend = GetComponent<Renderer>();
        Color color = Color.red;
        switch (health)
        {
            case 3:
                color = Color.green;
                break;
            case 2:
                color = Color.yellow;
                break;
            case 1:
                color = Color.red;
                break;
        }
        rend.material.color = color;
    }
}
