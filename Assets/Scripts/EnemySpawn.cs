﻿using System.Collections;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {

    public GameObject ground;
    public int maxNumberOfEnemiesToSpawn = 10;
    public int minNumberOfEnemiessToSpawn = 5;
    public float minSpawnHeight = 4f;
    public float maxSpawnHeight = 8f;
    public float spawnRate = 0.5f;
    public GameObject cubePrefab;
    public GameObject spherePrefab;

    private Renderer groundRenderer;
    private Coroutine spawnRoutine;

	void Start () {
        groundRenderer = ground.GetComponent<Renderer>();
        spawnRoutine = StartCoroutine(OnCoroutine());
    }

    public void StopSpawning()
    {
        if(spawnRoutine != null)
        {
            StopCoroutine(spawnRoutine);
        }
        spawnRoutine = null;
        
    }

    IEnumerator OnCoroutine()
    {
        while (true)
        {
            SpawnNewEnemies();
            yield return new WaitForSeconds(spawnRate);
        }
    }

    //Spawns between 3 and 5 enemies of random types at random coordinates whithin the ground plain
    private void SpawnNewEnemies()
    {
        int numberToSpawn = Random.Range(minNumberOfEnemiessToSpawn,maxNumberOfEnemiesToSpawn);
        for (int i = 0; i < numberToSpawn; i++)
        {
            GameObject prefab = i % 2 == 0 ? cubePrefab : spherePrefab;
            Instantiate(prefab, GenerateRandomPosition(prefab.transform), Quaternion.identity);
        }
    }

    private Vector3 GenerateRandomPosition(Transform transform)
    {
        Vector3 max = groundRenderer.bounds.max;
        Vector3 min = groundRenderer.bounds.min;
        float x = Random.Range(min.x + transform.localScale.x, max.x - transform.localScale.x);
        float y = Random.Range(minSpawnHeight, maxSpawnHeight);
        float z = Random.Range(min.z + transform.localScale.z,max.z - transform.localScale.z);

        return new Vector3(x, y, z);
    }
}
