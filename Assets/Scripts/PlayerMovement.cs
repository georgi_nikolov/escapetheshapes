﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float movementForce = 100f;
    public float topSpeed = 15f;
    public Rigidbody playerRb;

    private bool isMovingRight = false;
    private bool isMovingLeft = false;
    private bool isMovingForward = false;
    private bool isMovingBackward = false;
    private bool shouldProcessInput = true;

    void Update()
    {
        if (!shouldProcessInput)
        {
            isMovingLeft = false;
            isMovingRight = false;
            isMovingForward = false;
            isMovingBackward = false;
            return;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            isMovingForward = true;
        }
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            isMovingForward = false;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            isMovingBackward = true;
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            isMovingBackward = false;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            isMovingLeft = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            isMovingLeft = false;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            isMovingRight = true;
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            isMovingRight = false;
        }
    }

    private void FixedUpdate()
    {
        if (isMovingForward)
        {
            playerRb.AddForce(movementForce * Time.deltaTime, 0, 0, ForceMode.Impulse);
        }
        if (isMovingBackward)
        {
            playerRb.AddForce(-movementForce * Time.deltaTime, 0, 0, ForceMode.Impulse);
        }
        if (isMovingLeft)
        {
            playerRb.AddForce(0, 0, movementForce * Time.deltaTime, ForceMode.Impulse);
        }
        if (isMovingRight)
        {
            playerRb.AddForce(0, 0, -movementForce * Time.deltaTime, ForceMode.Impulse);
        }
        //Cap the speed 
        if (playerRb.velocity.magnitude > topSpeed)
        {
            playerRb.velocity = playerRb.velocity.normalized * topSpeed;
        }
            
    }

    public void StopReceivingInput()
    {
        this.shouldProcessInput = false;
    }


}
