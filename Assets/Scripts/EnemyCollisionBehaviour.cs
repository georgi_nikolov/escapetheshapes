﻿using UnityEngine;

public class EnemyCollisionBehaviour : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag.Equals("Ground") 
            || collision.collider.tag.Equals("Player"))
        {
            Destroy(gameObject);
        }
    }
}
