﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleSpriteSwap : MonoBehaviour {

    public Toggle toggle;

    void Awake()
    {
        toggle.onValueChanged.AddListener(OnTargetToggleValueChanged);
    }

    void OnEnable()
    {
        toggle.targetGraphic.enabled = !toggle.isOn;
    }

    void OnTargetToggleValueChanged(bool on)
    {
        toggle.targetGraphic.enabled = !on;
    }
}
